package anagrams.model;

import java.util.List;

public class WordList { // Bean made to pass words as an array

       List<String> words;

	public List<String> getWords() {
		return words;
	}

	public void setWords(List<String> words) {
		this.words = words;
	}

      
}