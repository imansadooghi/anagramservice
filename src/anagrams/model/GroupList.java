package anagrams.model;

import java.util.List;

public class GroupList { // Bean made to pass words as an array

	List<List<String>> groups;

	public List<List<String>> getGroups() {
		return groups;
	}

	public void setGroups(List<List<String>> groups) {
		this.groups = groups;
	}



      
}