package anagrams.model;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


public class Corpus implements ServletContextListener{
	//private static final String filePath = "./dictionary.txt";
	private static File f = new File("AnagramService/src/dictionary.txt");
	private static Corpus corpus;
	private static HashMap<String,List<String>> dict = new HashMap(); 
	private static TreeMap<Integer,List<String>> buckets = new TreeMap(); 
	private static int min=Integer.MAX_VALUE, max=0, median=0, average=0, sum=0;
	private static ArrayList<Integer> lengths = new ArrayList();
	

	public Corpus(){
		
	}
	static {
		corpus= new Corpus();
		initDict();
		initBuckets();
	}
	public static Corpus getCorpus(){
		return corpus;
	}
	
	
	private static void initDict(){ // this assumes there is no duplicate word in the dictionary input
		try (BufferedReader br = new BufferedReader(new FileReader(f.getAbsolutePath()))) {

		    String word;
		    while ((word = br.readLine()) != null) {
	            char[] ca = word.toLowerCase().toCharArray();
	            Arrays.sort(ca);
	            String key = String.valueOf(ca);
	            if (!dict.containsKey(key)) dict.put(key, new ArrayList<String>());
	            dict.get(key).add(word);
	            int len=key.length();
	            sum+=len;
	            lengths.add(len);//add unsorted. sort after done with init
		    }
		    Collections.sort(lengths);// now sort it. 
		    updateStats();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        System.out.println("dict initialized");

	}

	void insertLength(int num){
		int pos=Collections.binarySearch(lengths, num);	
	    if (pos < 0) 
	        lengths.add(-pos-1, num);
	    else
	    	lengths.add(pos,num);  
	}
	void removeLength(int num, int count){ // removes the num for count many times
		int pos=Collections.binarySearch(lengths, num);	
	    if (pos >= 0) 
	    		lengths.subList(pos, pos+count).clear();
	}
	void removeLength(int num){ 
		int pos=Collections.binarySearch(lengths, num);	
	    if (pos >= 0) 
	    		lengths.remove(pos);
	}	
	private static void initBuckets(){ // 
		for (String key : dict.keySet()){
			int count = dict.get(key).size();//how many anagrams
            if (!buckets.containsKey(count)) 
            	buckets.put(count, new ArrayList<String>());
			buckets.get(count).add(key);
		}
	}
	
	public List<String> getAnagrams(String word){
        char[] ca = word.toLowerCase().toCharArray();
        Arrays.sort(ca);
        String sorted = String.valueOf(ca);
        List<String> results= (dict.containsKey(sorted))? dict.get(sorted):new ArrayList<String>();
        results.remove(word);
		return results; 
	}
	
	public void addWords(List<String> words){ 
	    for (String word : words) {
            char[] ca = word.toLowerCase().toCharArray();
            Arrays.sort(ca);
            String key = String.valueOf(ca);
            if (!dict.containsKey(key)) 
            	dict.put(key, new ArrayList<String>());
            List<String> tempList =dict.get(key);
            if (!tempList.contains(word)){
            	int anagramCount=tempList.size();
            	tempList.add(word);
            	if (!buckets.containsKey(anagramCount)) buckets.put(anagramCount, new ArrayList<String>()); 
            	buckets.get(anagramCount).remove(key); //remove from old bucket 
            	if (!buckets.containsKey(anagramCount+1)) buckets.put(anagramCount+1, new ArrayList<String>());
            	buckets.get(anagramCount+1).add(key);  // add to next bucket!
            	int len=word.length();
	            insertLength(len);//add sorted
	            sum+=len;
              }
	    }
        updateStats();
	}
	private static void updateStats(){
		int count = lengths.size();
        min=lengths.get(0);
        max=lengths.get(count-1);
        average=sum/count;
        median= (count%2==1) ? 
        		lengths.get((count-1)/2):
        			(lengths.get((count-1)/2)+lengths.get((count-1)/2 +1))/2;
	}
	public boolean deleteWord(String word){ // returns false if word not existed.
		boolean wordExisted=false;
		char[] ca = word.toLowerCase().toCharArray();
        Arrays.sort(ca);
        String key = String.valueOf(ca);
        if (dict.containsKey(key)){
        	List<String> tempList=dict.get(key);
        	int anagramCount = tempList.size();
        	buckets.get(anagramCount).remove(key); //remove from old bucket
        	if (anagramCount>1)
        		buckets.get(anagramCount-1).add(key);  // add to bucket of 1 less anagrams!
        	wordExisted=tempList.remove(word);
        	removeLength(key.length());  // remove it from the lengths list
        	if (tempList.size()==0){
        		dict.remove(key);
        	}
            updateStats();
        }
		return wordExisted;
	}
	public void deleteAll() {
		dict=new HashMap();
		lengths = new ArrayList();
		buckets = new TreeMap();
		updateStats();
	}
	
	public boolean deleteAnagrams(String word){ // returns false if word not existed.  //todo: buckets modify
		boolean listExisted=false;
		char[] ca = word.toLowerCase().toCharArray();
        Arrays.sort(ca);
        String key = String.valueOf(ca);
        if (dict.containsKey(key)){
        	int anagramCount=dict.get(key).size();
        	dict.remove(key);
        	buckets.get(anagramCount).remove(key);
        	removeLength(key.length(),anagramCount);
        	sum-= key.length() * anagramCount;
        	updateStats();
        }
		return listExisted;
	}
	
	public boolean checkAnagrams(List<String> words){ // returns false if words are not eachother's anagram
		if (words==null || words.size()<2)
			return true;
		char[] ca = words.get(0).toLowerCase().toCharArray();
        Arrays.sort(ca);
        String base=String.valueOf(ca);
        
        for (int i = 1; i < words.size(); i++) {
        	String cand = words.get(i);
        	if (cand.length() != base.length())
        		return false;
    		char[] ar = cand.toLowerCase().toCharArray();
            Arrays.sort(ar);
            if(!String.valueOf(ar).equals(base))
            	return false;			
		}
		return true; 
	}
	public List<List<String>> getTopFrequency(){
		List<List<String>> result= new ArrayList();
		List<String> maxWords=buckets.get(buckets.lastKey());
		for (String key : maxWords)
			result.add(dict.get(key));
		return result;
	}
	
	public  List<List<String>> getHighFrequency(int k){
		List<List<String>> result= new ArrayList();
		int lastKey =buckets.lastKey();
		if (k>lastKey) // out of range frequencies
			return result;
		for (int frequency : buckets.tailMap(buckets.ceilingKey(k)).keySet()){
			List<String> bucketKeys=buckets.get(frequency);
			for (String key : bucketKeys)
				result.add(dict.get(key));
		}
		return result;
	}
	public int getTotalCount(){
		return lengths.size();
	}
	public String getStats(){
		return "Total count: "+lengths.size()+" maximum word length: "+max+
				" minimum word length: "+min+" average word length: "+average+" median word length: "+median;
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
	}
}
