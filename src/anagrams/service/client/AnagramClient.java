package anagrams.service.client;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import anagrams.model.GroupList;
import anagrams.model.WordList;

import com.google.gson.Gson;

public class AnagramClient {
       public static final String baseUri = "http://localhost:8080/AnagramService/";
       private Client client = null;
       private WebTarget target = null;
      
       public AnagramClient() {
              client = ClientBuilder.newClient();
              target = client.target(baseUri);
       }
      
       public void reloadUri() {
              target = null;
              target = client.target(baseUri);
       }
      
       public void getRequest() {
			reloadUri();
			target = target.path("/anagrams/main.json");
			// GET Request from Jersey Client
			Response response =  target.request(MediaType.APPLICATION_JSON)  
			                     .get(Response.class);                                
			if(response.getStatus() == 200) {
			       WordList anagrams = response.readEntity(new GenericType<WordList>() {});
			       System.out.println(anagrams.getWords());
			}
           System.out.println(response);

       }
       public void getTopFrequency() {
			reloadUri();
			target = target.path("/anagrams/topFrequency");
			// GET Request from Jersey Client
			Response response =  target.request(MediaType.APPLICATION_JSON)  
			                     .get(Response.class);                                
			if(response.getStatus() == 200) {
			       GroupList anagrams = response.readEntity(new GenericType<GroupList>() {});
			       System.out.println(anagrams.getGroups());
			}
           System.out.println(response);

       }   
       public void getCount() {
			reloadUri();
			target = target.path("/anagrams/count");
			// GET Request from Jersey Client
			Response response =  target.request(MediaType.APPLICATION_JSON)  
			                     .get(Response.class);                                
			if(response.getStatus() == 200) {
			       int count = response.readEntity(new GenericType<Integer>() {});
			       System.out.println(count);
			}
           System.out.println(response);

       } 
       public void getStats() {
			reloadUri();
			target = target.path("/anagrams/stats");
			// GET Request from Jersey Client
			Response response =  target.request(MediaType.APPLICATION_JSON)  
			                     .get(Response.class);                                
			if(response.getStatus() == 200) {
			       String stats = response.readEntity(new GenericType<String>() {});
			       System.out.println(stats);
			}
           System.out.println(response);

       } 
       
       public void getHighFrequency() {
			reloadUri();
			target = target.path("/anagrams/highFrequency/8");
			// GET Request from Jersey Client
			Response response =  target.request(MediaType.APPLICATION_JSON)  
			                     .get(Response.class);                                
			if(response.getStatus() == 200) {
			       GroupList anagrams = response.readEntity(new GenericType<GroupList>() {});
			       System.out.println(anagrams.getGroups());
			}
           System.out.println(response);

       }  
       public void getRequest(int limit, boolean properNouns) {
    	   reloadUri();
           target = target.path("/anagrams/Nami.json").queryParam("limit", limit).queryParam("nouns", properNouns);
           // GET Request from Jersey Client
           Response response =  target.request(MediaType.APPLICATION_JSON)  
                                .get(Response.class);                                
           if(response.getStatus() == 200) {
                  WordList anagrams = response.readEntity(new GenericType<WordList>() {});
                  System.out.println(anagrams.getWords());
           }
           System.out.println(response);

    }

       public void postList() {
           reloadUri();
           target = target.path("/words.json");
           List<String> list = Arrays.asList("Iman", "Iran", "Nami"); 
           WordList anagrams = new WordList();
           anagrams.setWords(list);
           Gson gson = new Gson();
           String input = gson.toJson(anagrams);

           
           //POST Request from jersey Client Using GSON
           Response response = target.request(MediaType.APPLICATION_JSON)
            .post(Entity.entity(input, MediaType.APPLICATION_JSON),Response.class);
           System.out.println(response.getStatusInfo());
           System.out.println(response);
       }
       
       public void checkAnagram() {
           reloadUri();
           target = target.path("/anagramCheck/words.json");
           List<String> list = Arrays.asList("Iman", "mIan", "Nami"); 
           WordList anagrams = new WordList();
           anagrams.setWords(list);
           Gson gson = new Gson();
           String input = gson.toJson(anagrams);

           
           //POST Request from jersey Client Using GSON
           //Response response = target.request(MediaType.APPLICATION_JSON)
            //.get(Response.class);
           
           Response response = target.request(MediaType.APPLICATION_JSON)
                   .put(Entity.entity(input, MediaType.APPLICATION_JSON),Response.class);
           if(response.getStatus() == 200) {
               boolean ans = response.readEntity(new GenericType<Boolean>() {});
               System.out.println(ans);
        }
           System.out.println(response);
       }      
       public void putRequest() {
              reloadUri();
              target = target.path("putMessage");
              String input ="{\"site\":\"www.9threes.com\",\"message\":\"is new domain\"}";
              
              //PUT Request from Jersey Client Example
              Response response = target.request(MediaType.APPLICATION_JSON)
               .put(Entity.entity(input, MediaType.APPLICATION_JSON),Response.class);
             
              System.out.println(response);
              if(response.getStatus() == 200) {
                     System.out.println("put request using Json is Success");
              }
       }
      
       public static void main(String args[]) throws Exception {
              try {
                     AnagramClient jerseyClient = new AnagramClient();
                     jerseyClient.getCount();
                     jerseyClient.postList();
                     jerseyClient.getRequest();
                     jerseyClient.getRequest(2,true);
                     jerseyClient.getRequest(102,false);
                     jerseyClient.checkAnagram();
                     jerseyClient.getTopFrequency();
                     jerseyClient.getHighFrequency();
                     jerseyClient.getCount();
                     jerseyClient.getStats();
              } catch(Exception e) {
                     System.out.println(e);
              }
       }

}
