package anagrams.service;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import anagrams.model.Corpus;
import anagrams.model.GroupList;
import anagrams.model.WordList;
@Path("/")
public class AnagramServlet {
    Corpus corpus=Corpus.getCorpus();

    @POST
    @Path("/words.json")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postList(WordList anagrams) {
      	   corpus.addWords(anagrams.getWords());
          return Response.status(200).build();
    }
    
    @GET
    @Path("/anagrams/{word}.json")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public WordList getAnagrams(@PathParam("word") String word, 
    		@DefaultValue("-1") @QueryParam("limit") int limit, 
    		@DefaultValue("true") @QueryParam("nouns") boolean properNouns) { 

    	WordList anagrams= new WordList();
    	List<String> tempResults = corpus.getAnagrams(word);
    	List<String> results;
    	if (!properNouns){
    		results=new ArrayList<String>();
    		for (String noun : tempResults){
    			if (Character.isLowerCase(noun.codePointAt(0)) || Character.isLowerCase(noun.charAt(0)))
    				results.add(noun);	
    		}		
    	} else
    		results=tempResults;
    	
    	if (limit >=0 && limit <= results.size())
    		anagrams.setWords(results.subList(0, limit));
    	else 
    		anagrams.setWords(results);

    	return anagrams;
    }
    
    @DELETE
    @Path("/words/{word}.json")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteWord(@PathParam("word") String word) { // returns 200 for successful delete and 404 for a non-existing word delete
    	
    	boolean result = corpus.deleteWord(word);
        return (result)?Response.status(200).build():Response.status(404).build();
    }
    
    @DELETE
    @Path("/words.json")
    public Response deleteAll() { 
    	corpus.deleteAll();
    	return Response.status(204).build();
    }
    
    @DELETE
    @Path("/anagrams/{word}.json")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteAnagrams(@PathParam("word") String word) { // returns 200 for successful delete and 404 for a non-existing word delete
    	boolean result = corpus.deleteAnagrams(word);
        return (result)?Response.status(200).build():Response.status(404).build();
    }
    @PUT
    @Path("/anagramCheck/words.json")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response checkAnagrams(WordList anagrams) { //check if the list of words are anagrams        
      	boolean result =corpus.checkAnagrams(anagrams.getWords());
      	return Response.status(200).entity(result).build();
    }
    
    @GET
    @Path("/anagrams/topFrequency")
    @Produces(MediaType.APPLICATION_JSON)
    public GroupList getMaxAnagrams() { 
    	GroupList result = new GroupList();
    	result.setGroups(corpus.getTopFrequency());
    	return result;
    }
    @GET
    @Path("/anagrams/count")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCount() { 
    	return Response.status(200).entity(corpus.getTotalCount()).build();    
    }
    
    @GET
    @Path("/anagrams/stats")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStats() { 
    	return Response.status(200).entity(corpus.getStats()).build();    
    }
    
    @GET
    @Path("/anagrams/highFrequency/{k}")
    @Produces(MediaType.APPLICATION_JSON)
    public GroupList getHighFrequency(@PathParam("k") int k) { 
    	GroupList result = new GroupList();
    	result.setGroups(corpus.getHighFrequency(k));
    	return result;
    }
}   

