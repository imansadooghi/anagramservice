AnagramService Project

---

The project is an API that allows fast searches for [anagrams](https://en.wikipedia.org/wiki/Anagram). `dictionary.txt` is a text file containing every word in the English dictionary.

Here is the full list of functionalities provided by this API:

- http://localhost:8080/AnagramService/words.json (post)
loads the dictionary with a list of new words.

- http://localhost:8080/AnagramService/anagrams/:word.json 
returns the list of anagrams for the word. maximum number of answers and inclusion of proper nouns could be determined.

- http://localhost:8080/AnagramService/words/:word.json (delete)
delete a word in dictionary.

- http://localhost:8080/AnagramService/words.json (delete)
delete all of the dictionary contents.

- http://localhost:8080/AnagramService/count    
returns total number of words in dictionary

- http://localhost:8080/AnagramService/stats    
returns count of words in the corpus and min/max/median/average word length

- http://localhost:8080/AnagramService/anagrams/topFrequency 
returns words with the most anagrams     						

- http://localhost:8080/AnagramService/anagramCheck/words.json (put)
takes a set of words and returns whether or not they are all anagrams of each other

- http://localhost:8080/AnagramService/anagrams/highFrequency/x 
return all anagram groups of size >= *x*		
					
- http://localhost:8080/AnagramService/anagrams/:word.json (delete)
delete a word *and all of its anagrams*        


Examples (assuming the API is being served on localhost port 8080):  
example end-point: http://localhost:8080/AnagramService/
```{bash}

# Adding words to the corpus
$ curl -i -X POST -H "Content-Type:application/json" -d '{ "words": ["read", "dear", "dare"] }'  http://localhost:8080/AnagramService/words.json 

HTTP/1.1 201 Created
...

# Fetching anagrams
$ curl -i  http://localhost:8080/AnagramService/read.json
HTTP/1.1 200 OK
...
{
  anagrams: [
    "dear",
    "dare"
  ]
}

# Specifying maximum number of anagrams
$ curl -i  http://localhost:8080/AnagramService/read.json?limit=1
HTTP/1.1 200 OK
...
{
  anagrams: [
    "dare"
  ]
}


# Specifying maximum number of anagrams  and specifying not to include proper nouns in answer
# Proper nouns in this service are defined as the words that start with capital letters.
$ curl -i  http://localhost:8080/AnagramService/read.json?limit=1&nouns=false
HTTP/1.1 200 OK
...
{
  anagrams: [
    "dare"
  ]
}

# Delete single word
$ curl -i -X DELETE http://localhost:8080/AnagramService/words/read.json
HTTP/1.1 200 OK
...

# Delete the word and all of its anagrams
$ curl -i -X DELETE http://localhost:8080/AnagramService/anagrams/read.json
HTTP/1.1 200 OK

# Delete all words
$ curl -i -X DELETE http://localhost:8080/AnagramService/words.json
HTTP/1.1 204 No Content
...
```

Notes: Please make sure to have the dictionary file in the src directory of the working projects